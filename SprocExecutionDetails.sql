SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

DECLARE @SpName          varchar(255) = 'GeneratedimAppointmentReferrals_DV';

SELECT procedurename = OBJECT_NAME(ps.object_id, ps.database_id),
       procedureexecutes = ps.execution_count,
       versionofplan = qs.plan_generation_num,
       executionsofcurrentplan = qs.execution_count,
       [query plan xml] = qp.query_plan
FROM sys.dm_exec_procedure_stats AS ps
JOIN sys.dm_exec_query_stats AS qs
  ON ps.plan_handle = qs.plan_handle
CROSS APPLY sys.dm_exec_query_plan(qs.plan_handle) AS qp
WHERE ps.database_id = DB_ID()
      AND OBJECT_NAME(ps.object_id, ps.database_id) = @SpName;